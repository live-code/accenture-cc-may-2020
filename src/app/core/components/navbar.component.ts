import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ThemeService } from '../settings/theme.service';

@Component({
  selector: 'app-navbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div 
      class="p-4"
      [ngClass]="{
        'bg-dark': theme === 'dark',
        'bg-light': theme === 'light'
      }"
    >
      <button routerLink="home" routerLinkActive="bg-warning">home</button>
      <button routerLink="catalog" routerLinkActive="bg-warning">catalog</button>
      <button routerLink="settings" routerLinkActive="bg-warning">settings</button>
      <button routerLink="login" routerLinkActive="bg-warning">login</button>
    </div>
    {{render()}}
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {
  @Input() theme: string;

  ngOnInit(): void {
  }

  render() {
    console.log('render navbar')
  }
}
