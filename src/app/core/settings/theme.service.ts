import { Injectable } from '@angular/core';

@Injectable()
export class ThemeService {
  theme = 'dark';

  setTheme(value: string) {
    this.theme = value;
  }

}

