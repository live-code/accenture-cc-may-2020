import { Component } from '@angular/core';
import { ThemeService } from './core/settings/theme.service';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar [theme]="themeService.theme"></app-navbar>
    
    <div class="container mt-3">
      <router-outlet></router-outlet>
    </div>
      
  `,
})
export class AppComponent  {
  constructor(public themeService: ThemeService) {}

}

