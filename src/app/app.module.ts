import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CatalogComponent } from './features/catalog/catalog.component';
import { HomeComponent } from './features/home/home.component';
import { LoginComponent } from './features/login/login.component';
import { SettingsComponent } from './features/settings/settings.component';
import { RouterModule } from '@angular/router';
import { CatalogDetailsComponent } from './features/catalog-details/catalog-details.component';
import { NavbarComponent } from './core/components/navbar.component';
import { ThemeService } from './core/settings/theme.service';
import { HelloComponent } from './shared/components/hello.component';
import { CardComponent } from './shared/components/card.component';
import { TabbarComponent } from './shared/components/tabbar.component';
import { CatalogListComponent } from './features/catalog/components/catalog-list.component';
import { CatalogFormComponent } from './features/catalog/components/catalog-form.component';
import { CatalogErrorComponent } from './features/catalog/components/catalog-error.component';

@NgModule({
  declarations: [
    AppComponent,
    CatalogComponent,
    HomeComponent,
    LoginComponent,
    SettingsComponent,
    CatalogDetailsComponent,
    NavbarComponent,
    HelloComponent,
    CardComponent,
    TabbarComponent,
    CatalogListComponent,
    CatalogFormComponent,
    CatalogErrorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent },
      { path: 'login', component: LoginComponent },
      { path: 'catalog', component: CatalogComponent },
      { path: 'catalog/:id', component: CatalogDetailsComponent},
      { path: 'settings', component: SettingsComponent },
      { path: '', component: HomeComponent },
      { path: '**', redirectTo: 'home' },
    ])
  ],
  providers: [
    ThemeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
