import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <div [style.color]="color">
      hello {{name}}!
      
      <button 
        *ngIf="actionLabel"
        (click)="actionClick.emit()"
        >
        {{actionLabel}}
      </button>
    </div>
  `,
})
export class HelloComponent {
  @Input() name = 'guest';
  @Input() color = 'green';
  @Input() actionLabel: string;
  @Input() url: string;
  @Output() actionClick
    = new EventEmitter();
}

