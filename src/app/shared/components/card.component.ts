import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card" [ngClass]="'mb-' + marginBottom">
      <div 
        class="card-header"
        (click)="open = !open"
        [class]="headerBg"
      >
        {{title}}
        
        <div class="pull-right">
          <i 
            *ngIf="icon"
            [class]="'fa fa-' + icon"
            (click)="iconHandler($event)"
          ></i>
        </div>
      </div>
      
      <div class="card-body" *ngIf="open">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: string;
  @Input() icon: string;
  @Input() marginBottom: string;
  @Input() headerBg = 'bg-dark text-white';
  @Output() iconClick: EventEmitter<void> = new EventEmitter<void>();
  open = true;

  iconHandler(event: MouseEvent) {
    event.stopPropagation()
    this.iconClick.emit();
  }
}
