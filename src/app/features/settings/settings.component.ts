import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/settings/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    {{themeService.theme}}
    <button (click)="changeThemeHandler('dark')">Dark</button>
    <button (click)="changeThemeHandler('light')">light</button>
  `,
})
export class SettingsComponent {

  constructor(public themeService: ThemeService) {
  }

  changeThemeHandler(value: string) {
    this.themeService.setTheme(value)
  }
}
