import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  template: `
    <app-hello 
      name="Fabio Biondi.io" 
      color="red"
      actionLabel="Visit site"
      (actionClick)="doAction('http://www.fabiobiondi.io')"
    ></app-hello>

    <app-card [marginBottom]="'bubu'"></app-card>

    <app-card
      title="Stats"
      icon="bar-chart"
      [marginBottom]=""
    >
      <div class="row">
        <div class="col">
          <app-card title="1">111</app-card>
        </div>
        <div class="col">
          <app-card title="2">222</app-card>
        </div>
      </div>
    </app-card>
    
    <app-card 
      title="Profile"
      icon="user"
      headerBg="bg-success"
      (iconClick)="doAction('http://www.linkedin.com/')"
    >
      <app-catalog></app-catalog>
    </app-card>

    
  `,
})
export class HomeComponent   {
  doAction(url: string) {
    window.open(url)
  }
}
