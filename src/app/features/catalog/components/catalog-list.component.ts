import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Device } from '../model/device';

@Component({
  selector: 'app-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      class="list-group-item"
      *ngFor="let device of devices"
      [ngClass]="{'active': device.id === active?.id}"
      (click)="setActive.emit(device)"
    >
      {{device.label}} ({{device.id}})

      <i
        class="fa"
        [ngClass]="{
            'fa-android': device.os === 'android',
            'fa-apple': device.os === 'ios'
          }"
      ></i>

      <div class="pull-right" [style.color]="device.price >= 1000 ? 'red' : null">
        € {{device.price}}

        <i class="fa fa-trash" (click)="deleteHandler(device, $event)"></i>

        <i class="fa fa-info-circle" [routerLink]="'/catalog/' + device.id"></i>
        <i class="fa fa-info-circle" (click)="gotoHandler(device.id, $event)"></i>

      </div>
    </li>
    
    {{render()}}
  `,
})
export class CatalogListComponent  {
  @Input() devices: Device[];
  @Input() active: Device;
  @Output() setActive: EventEmitter<Device> = new EventEmitter<Device>();
  @Output() delete: EventEmitter<Device> = new EventEmitter<Device>();
  @Output() goto: EventEmitter<number> = new EventEmitter<number>();

  deleteHandler(device: Device, event: MouseEvent) {
    event.stopPropagation();
    this.delete.emit(device);
  }

  gotoHandler(id: number, event: MouseEvent) {
    event.stopPropagation();
    this.goto.emit(id);
  }

  render() {
    console.log('render')
  }
}
