import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Device } from '../model/device';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-catalog-form',
  template: `
    
    <form
      #f="ngForm"
      (submit)="saveHandler()"
    >

      <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">
              <i
                class="fa"
                [ngClass]="{
                  'fa-check': labelInput.valid,
                  'fa-asterisk': labelInput.errors?.required,
                  'fa-sort-numeric-asc': labelInput.errors?.minlength
                }"
                [style.color]="labelInput.valid ? 'green' : 'red'"
              ></i>
            </span>
        </div>
        <input
          type="text"
          name="label"
          [ngModel]="activeDevice?.label"
          class="form-control"
          [ngClass]="{
              'is-invalid': labelInput.invalid && f.dirty,
              'is-valid': labelInput.valid
            }"
          #labelInput="ngModel"
          required
          minlength="3"
          placeholder="Device name"
        >
      </div>

      <div class="form-group">
        <input
          type="text"
          name="price"
          [ngModel]="activeDevice?.price"
          class="form-control"
          [ngClass]="{
                'is-invalid': labelPrice.invalid && f.dirty,
                'is-valid': labelInput.valid
              }"
          #labelPrice="ngModel"
          required
          placeholder="Device Price"
        >
      </div>

      <select
        [ngModel]="activeDevice?.os"
        name="os" class="mb-3 form-control" required
        #inputOs="ngModel"
        [ngClass]="{
            'is-invalid': inputOs.invalid && f.dirty,
            'is-valid': inputOs.valid
          }"
      >
        <option [ngValue]="null">Select OS</option>
        <option value="ios">apple</option>
        <option value="android">android</option>
      </select>


      <div class="btn-group btn-block">
        <button
          class="btn btn-primary"
          type="submit" [disabled]="f.invalid">
          {{activeDevice?.id ? 'EDIT' : 'ADD'}}
        </button>

        <button class="btn btn-outline-primary" type="button"
                (click)="resetHandler()">CLEAR</button>
      </div>
    </form>
  `,
  styles: [
  ]
})
export class CatalogFormComponent {
  @ViewChild('f', { static: true }) form: NgForm;

  @Input() set active(val: Device) {
    this.activeDevice = val;
    if (val && !val.id) {
      this.form.reset();
    }
  }

  activeDevice: Device;

  @Output() clean: EventEmitter<void> = new EventEmitter();
  @Output() save: EventEmitter<Device> = new EventEmitter();

  resetHandler() {
    this.form.reset();
    this.clean.emit();
  }

  saveHandler() {
    this.save.emit(this.form.value)
  }

}
