import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-catalog-error',
  template: `
    <div class="alert alert-danger" (click)="closeAlert.emit()">
      C'è un errore
    </div>
  `,
})
export class CatalogErrorComponent {
  @Output() closeAlert: EventEmitter<void> = new EventEmitter<void>()
}
