export interface Device {
  id: number;
  label: string;
  price: number;
  madeIn: string;
  os: string;
}
