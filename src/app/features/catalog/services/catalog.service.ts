import { Injectable } from '@angular/core';
import { Device } from '../model/device';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  devices: Device[];
  active: Device;
  error: boolean;

  constructor(private http: HttpClient) {}

  loadAll()  {
    this.http.get<Device[]>('http://localhost:3000/devices')
      .subscribe((res) => {
        this.devices = res;
        this.clean();
      });
  }

  delete(device: Device) {
    this.http.delete(`http://localhost:3000/devices/${device.id}`)
      .subscribe(
        () => {
          const index = this.devices.findIndex(d => d.id === device.id)
          // this.devices.splice(index, 1);
          this.devices = this.devices.filter(d => d.id !== device.id)

          if (device.id === this.active?.id) {
            this.clean();
          }
        },
        (err) => {
          this.error = true;
          setTimeout(() => this.error = false, 1000);
        }
      );
  }


  save(device: Device): Promise<Device> {
    if (this.active && this.active.id) {
      this.editHandler(device);
    } else {
      return this.addHandler(device);
    }
  }

  addHandler(device: Device): Promise<Device> {
    return new Promise((resolve, reject) => {
      this.http.post<Device>('http://localhost:3000/devices', device)
        .subscribe(res => {
          this.devices = [...this.devices, res];
          this.clean();
          resolve(res);
        });
    });
  }

  editHandler(device: Device) {
    this.http.patch<Device>('http://localhost:3000/devices/' + this.active.id, device)
      .subscribe(res => {
        // const index = this.devices.findIndex(d => d.id === this.active.id);
        // this.devices[index] = res;
        this.devices = this.devices.map(d => {
          return d.id === this.active.id ? res : d;
        });
      });
  }

  setActive(device: Device) {
    this.active = {...device};
  }

  clean() {
    this.active = { os: null } as Device;
  }

}
