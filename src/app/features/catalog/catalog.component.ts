import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CatalogService } from './services/catalog.service';

@Component({
  selector: 'app-catalog',
  template: `

    <app-catalog-error 
      *ngIf="catalogService.error"
      (closeAlert)="catalogService.error = false"
    ></app-catalog-error>
    
    <div class="container mt-3">
      <app-catalog-form
        [active]="catalogService.active"
        (save)="catalogService.save($event)"
        (clean)="catalogService.clean()"
      ></app-catalog-form>
  
      <hr>
       
     <app-catalog-list
       [devices]="catalogService.devices"
       [active]="catalogService.active"
       (setActive)="catalogService.setActive($event)"
       (delete)="catalogService.delete($event)"
       (goto)="gotoHandler($event)"
     ></app-catalog-list>
    </div>
  `,
})
export class CatalogComponent {
  constructor(private router: Router, public catalogService: CatalogService) {
    this.catalogService.loadAll();
  }

  gotoHandler(id: number) {
    this.router.navigateByUrl('catalog/' + id)
  }
}

