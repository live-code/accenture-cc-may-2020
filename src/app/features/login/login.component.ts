import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/settings/theme.service';

@Component({
  selector: 'app-login',
  template: `
    <app-tabbar 
      [items]="countries"
      [active]="selectedCountry"
      (tabClick)="selectCountry($event)"
    ></app-tabbar>

    <app-tabbar
      *ngIf="selectedCountry"
      labelField="name"
      [active]="selectedCity"
      [items]="selectedCountry.cities"
      (tabClick)="selectCity($event)"
    ></app-tabbar>
    
    
    <app-card 
      *ngIf="selectedCity" 
      [title]="selectedCity.name">
      
      <img [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + selectedCity.name + '&size=200,100'" alt="">
    </app-card>
    
  `,
})
export class LoginComponent {
  selectedCountry: Country;
  selectedCity: City;
  countries: Country[];

  constructor() {
    setTimeout(() => {
      this.countries = [
        { id: 1001, label: 'Japan', cities: [ { id: 1, name: 'Tokyo'}, { id: 2, name: 'Kyoto'}, { id: 3, name: 'Osaka'}, ],  lat: 45.6, lng: 13.77, temp: [12, 19, 3, 5, 2, 3]},
        { id: 1002, label: 'Italy', cities: [ { id: 2, name: 'Milano'}, { id: 5 , name: 'Rome'} ],  lat: 48.8, lng: 2.35, temp: [12, 19, 3, 35, 2, 3]},
        { id: 1003, label: 'Spain', cities: [ { id: 3, name: 'Madrid'} ],  lat: 40, lng: -74, temp: [12, 19, 23, 25, 12, 3]}
      ];
      this.selectCountry(this.countries[0]);
    }, 1000);
  }

  selectCountry(country: Country) {
    this.selectedCountry = country;
    this.selectedCity = country.cities[0];
  }

  selectCity(city: City) {
    this.selectedCity = city;
  }
}


// ---

export class Country {
  id: number;
  label: string;
  lat: number;
  lng: number;
  temp: Array<number>;
  cities: City[];
}

interface City {
  id: number;
  name: string;
}
