import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Device } from '../catalog/model/device';

@Component({
  selector: 'app-catalog-details',
  template: `
    {{device | json}}
    
    <h1>{{device?.label}}</h1>
    
    <button routerLink="/catalog/42">Next</button>
  `,
  styles: [
  ]
})
export class CatalogDetailsComponent {
  device: Device;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
  ) {

    activatedRoute.params
      .subscribe(res => {
        this.init(res.id);
      });

  }

  init(id: number) {
    this.http.get<Device>('http://localhost:3000/devices/' + id)
      .subscribe(res => {
        this.device = res
        console.log('init', res);
      });

  }


}
